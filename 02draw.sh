#!/bin/bash

echo "Drawing with plotrepo.rb"
#   apt -y install ruby-rugged   # to generate dot schema
#   apt -y install graphviz      # to render dot to png
ruby plotrepo.rb ./1 | dot -Tpng > plotrepo.1.png
ruby plotrepo.rb ./2 | dot -Tpng > plotrepo.2.png
