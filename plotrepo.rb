#!/usr/bin/env ruby
#
# Display a git repository.
#
# Install:
#
#   apt install ruby-rugged              # to generate dot schema
#   apt install graphviz imagemagick     # to render dot and display
#
# Use:
#
#   ruby plotrepo.rb ./path_to_repo | dot -Tpng | display -antialias
#
# Author: Noufal Ibrahim
# License: MIT
# URL: https://code.activestate.com/recipes/577572-display-a-git-repository/ 
#
# Changes:
#   - port from `grit` (Ruby 1.x) to `rugged` (Ruby 2.x)
#   - if HEAD references branch, use short branch name

require 'rugged'

$commits = {}

def commit_node_label(commit)
  return "#{commit.oid.slice 0,5}\\n(#{commit.message.split("\n")[0]})"
end

def plot_tree (commit)
  if $commits.has_key? commit.oid
    return
  else
    $commits[commit.oid] = 1
  
    commit.parents.each do |c|
      puts "\"#{commit_node_label commit}\" -> \"#{commit_node_label c}\";"
      plot_tree(c)
    end
  end
end

def plot_tags(repo)
  repo.tags.each do |tag|
    puts "\"#{tag.name}\" -> \"#{commit_node_label tag.commit}\";"
    puts "\"#{tag.name}\" [shape=box, style=filled, color = yellow];"
  end
end

def draw_head(repo)
  if repo.head.branch?
    head = repo.branches[repo.head.name].name
  else
    head = repo.head.name
  end
  puts "\"HEAD\"  [shape=box, style=filled, color = green];"
  puts "\"HEAD\" -> \"#{head}\";"
end
  
def draw_branch_heads(repo)
  repo.branches.each do |b| 
    puts "\"#{b.name}\" -> \"#{commit_node_label b.target}\";"
    puts "\"#{b.name}\" [shape=polygon, sides=6, style=filled, color = red];"
    plot_tree(b.target)
  end  
end

puts "Digraph F {"
puts 'ranksep=0.5; size = "17.5,7.5"; rankdir=RL;'
repo = Rugged::Repository.new(ARGV[0]);
draw_branch_heads(repo)
plot_tags(repo)
draw_head(repo)
puts "}"
