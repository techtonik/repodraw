#!/bin/bash
mkdir 1
mkdir 2
git -C 1 init
git -C 2 init

cd 1 || exit
echo "first" >> uno
git add uno
git commit -m "msg1"
echo "first 2" >> uno
git commit -am "msg2"
echo "first 3" >> uno
git commit -am "msg3"
cd ..

cd 2
echo "=== 1" >> z2.md
git add z2.md
git commit -m "echo#1"
echo "=== 2" >> z2.md
git commit -am "echo#2"
cd ..

# interleaved commits
echo "four" >> 1/uno
git -C 1 commit -am "msg4"
sleep 1
echo "=== 3" >> 2/z2.md
git -C 2 commit -am "echo#3"
sleep 1
echo "five" >> 1/uno
git -C 1 commit -am "msg5"
sleep 1
echo "=== 4" >> 2/z2.md
git -C 2 commit -am "echo#4"
sleep 1

echo "six" >> 1/uno
git -C 1 commit -am "msg6"
echo "seven" >> 1/uno
git -C 1 commit -am "msg7"

