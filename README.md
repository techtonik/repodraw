### The [missing](https://detunized.net/posts/2019-02-17-git-fu-reposurgeon) `reposurgeon` tutorial

Suppose you want to make one repository from two unrelated repos located in dirs `"1/"` and `"2/"`.

This is repo `"1"`:

![repo 1](plotrepo.1.png)

And this is repo `"2"`:

![repo 2](plotrepo.2.png)

First create a script with commands for `reposurgeon`. You can actually enter these commands
interactively if you launch `reposurgeon` without arguments. I named my script
`reposurgeon.script`:

```bash
# read first repo into memory
read 1
# read second repo into memory
read 2

# merge repos together
unite 1 2

# save the result into dir 3
rebuild 3
```

Now run `reposurgeon "script reposurgeon.script"` and you will get repo "3" with two branches.

![repo 3](plotrepo.3.png)

That's it.

#### Merging repositories without `reposurgeon

You can also merge repos using just plain `git`, like this:

```bash
$ git clone https://github.com/simons-public/protonfixes first
$ cd first
$ git remote add second https://github.com/ValveSoftware/steam-for-linux
$ git fetch --all
Fetching origin
Fetching second
...
From https://github.com/ValveSoftware/steam-for-linux
 * [new branch]      master     -> second/master
$ git checkout -b steam second/master
Branch 'steam' set up to track remote branch 'master' from 'second'.
Switched to a new branch 'steam'

$ git rebase master
CONFLICT (add/add): Merge conflict in README.md
Auto-merging README.md
...
```
We've got a merge conflict, but you get the idea. With `reposurgeon`
you could rename `README.md` on one of the repos to avoid the conflict.

#### Editing history with `reposurgeon` while merging

[`path`](http://www.catb.org/esr/reposurgeon/repository-editing.html#paths)
command can be used to move all files from the last selected repo into
`docs/` subdirectory.

```bash
read 1
read 2

path rename /.+/ docs/\0

unite 1 2
rebuild 3
```

For the description of various commands, see http://www.catb.org/~esr/reposurgeon/repository-editing.html

If `reposurgeon` helped you to lift or save some money, consider [supporting ESR](https://www.patreon.com/esr)
who've built and maintains this awesome tool.

---

#### Signed commits gotcha

If your repo contains signed commits, then such commits and all commits that follow it will change their
hashes. Which means that if you try to create a PR from your new repository to the old one, there might
be more changes that you've expected. In that case it is worth to add old repository as a remote and
rebase once more against it before submitting your PR.

For example, in the `protonfixes` repo the very first commit is signed. So to make PR that will not try
to duplicate commits that are already present under different hashes, the result of the merge (repo 3)
needs to be rebased against original upstream repo.

```bash
cd 3
# reposurgeon automatically renames the second master
git checkout master-2
git checkout -b branch4pr
git remote add upstream https://github.com/simons-public/protonfixes
git fetch --all
git rebase upstream/master
git push git@github.com:techtonik/protonfixes.git
```

There is little that can be done on the `reposurgeon` side to "fix" signed commits , but you can help
with upstream fix so that `reposurgeon` could at least warn you if commits hashes will change
https://gitlab.com/esr/reposurgeon/-/issues/367
